Installation and running
========================

**composer install**

Before run need install chromium browser

**sudo apt-get install chromium-browser**

After that you need to launch the browser **in a separate terminal**, in debug mode

**/usr/bin/chromium-browser --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222**

After that - run command

**vendor/bin/behat --config app/config/behat.yml src/AppBundle/Features/RetailmenotMatchPage.feature**

the received content will be saved in the folder /var/www/data, parameter save_path in parameters.yml