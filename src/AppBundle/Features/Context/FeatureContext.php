<?php

namespace AppBundle\Features\Context;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{

    private $kernel;

    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When /^I am on the main page/
     */
    public function iAmOnTheMainPage()
    {
        $this->visit('https://www.retailmenot.com/view/match.com');

        $date = new \DateTime();

        $path = $this->getContainer()->getParameter('save_path');
        $name = 'retailmenotMatch_'.$date->format('Y-m-d').'.html';

        file_put_contents($path.'/'.$name, '<html>'.$this->getSession()->getPage()->getContent().'</html>');
    }

    /**
     * @When /^I see the word "([^"]*)" somewhere on the page$/
     */
    public function iSeeTheWord($param)
    {
        $this->assertPageContainsText($param);
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }
}
