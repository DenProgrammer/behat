# language: en

Feature: Retrieving some page for the https://retailmenot.com

    Scenario: I see target page
    Given I am on the main page
    And I see the word "Top Offers" somewhere on the page
